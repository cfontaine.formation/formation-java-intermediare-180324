package fr.dawan.designpattern.structure.facade;

public class DecodeurAudio {

    public void Decoder(String fichier) {
        System.out.println("Decodage du fichier " + fichier);
    }
}
