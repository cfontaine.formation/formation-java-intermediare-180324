package fr.dawan.designpattern.structure.decorator;

public interface Voiture {
    void assembler();
}
