package fr.dawan.designpattern.structure.proxy;

public class App {

    public static void main(String[] args) {
        Message msg=new UserMessage("le contenu du message");
        Message p=new MessageProxy(msg);
        
        System.out.println(p.getContent());

    }

}
