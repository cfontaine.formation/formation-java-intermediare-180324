package fr.dawan.designpattern.structure.facade;

public class LecteurAudioFacade {
    
    private ChargeurFichier chargeurFichier;
    
    private DecodeurAudio decodeurAudio;
    
    private AmplificateurSon amplificateurSon;

    public LecteurAudioFacade() {
        chargeurFichier=new ChargeurFichier();
        decodeurAudio=new DecodeurAudio();
        amplificateurSon=new AmplificateurSon();
    }
    
    public void jouer(String fichier) {
        chargeurFichier.chargerFichier(fichier);
        decodeurAudio.Decoder(fichier);
        amplificateurSon.amplifier();
    }

}
