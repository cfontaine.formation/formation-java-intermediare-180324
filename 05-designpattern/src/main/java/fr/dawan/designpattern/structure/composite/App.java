package fr.dawan.designpattern.structure.composite;

public class App {

    public static void main(String[] args) {
       Manager m1=new Manager("Jo Dalton");
       Consultant c1=new Consultant("Yves roulo");
       Consultant c2=new Consultant("John Doe");
       Consultant c3=new Consultant("Jane Doe");
       Consultant c4=new Consultant("Jim Profit");
       m1.ajoutEmploye(c1);
       m1.ajoutEmploye(c2);
       m1.ajoutEmploye(c3);
       m1.ajoutEmploye(c4);
     m1.afficher();
    }

}
