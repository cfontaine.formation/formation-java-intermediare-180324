package fr.dawan.designpattern.structure.bridge;

public interface Device {

    boolean isEnabled();
    
    void setVolume(int v);
    
    void setChannel(int ch);
    
    void turnOn();
    
    void turnOff();
}
