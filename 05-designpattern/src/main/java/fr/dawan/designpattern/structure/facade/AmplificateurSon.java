package fr.dawan.designpattern.structure.facade;

public class AmplificateurSon {
    public void amplifier() {
        System.out.println("Amplification du son ");
    }
}
