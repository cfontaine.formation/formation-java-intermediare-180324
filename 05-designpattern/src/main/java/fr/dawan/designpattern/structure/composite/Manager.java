package fr.dawan.designpattern.structure.composite;

import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {
    
    private List<Employee> employes=new ArrayList<>();
    
    public Manager(String nom) {
        super(nom);
    }

    @Override
    public void afficher() {
        System.out.println(getNom());
        System.out.println("-------------");
        employes.forEach(e -> e.afficher());
    }

    public boolean ajoutEmploye(Employee employee) {
        return employes.add(employee);
    }
    
    public boolean retirerEmploye(Employee employee) {
        return employes.remove(employee);
    }
    
    public Employee getChild(int index) {
        return employes.get(index);
    }
}
