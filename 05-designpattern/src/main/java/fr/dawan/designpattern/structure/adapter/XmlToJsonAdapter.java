package fr.dawan.designpattern.structure.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;

public class XmlToJsonAdapter<T> implements Target {

    private XmlConvertable<T> xmlConverter;
    private Class<T> clazz;

    public XmlToJsonAdapter(XmlConvertable<T> xmlConverter, Class<T> clazz) {
        this.xmlConverter = xmlConverter;
        this.clazz = clazz;
    }

    @Override
    public String toJson(String xml) throws Exception {
        T obj = xmlConverter.fromXml(xml, clazz);
        ObjectMapper jsonMapper = new ObjectMapper();
        String json = jsonMapper.writeValueAsString(obj);
        return json;
    }

}
