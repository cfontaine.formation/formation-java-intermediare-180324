package fr.dawan.designpattern.structure.decorator;

public class App {

    public static void main(String[] args) {
        Voiture v1=new VoitureSportDecorator(new VoitureSimple());
        v1.assembler();
    }

}
