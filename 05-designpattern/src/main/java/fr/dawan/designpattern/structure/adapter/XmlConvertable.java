package fr.dawan.designpattern.structure.adapter;

public interface XmlConvertable<T> {

    String toXml(T obj) throws Exception;

    T fromXml(String xml, Class<T> clazz) throws Exception;
}
