package fr.dawan.designpattern.structure.composite;

public abstract class Employee {

    private String nom;
    
    public Employee(String nom) {
        this.nom = nom;
    }

    public abstract void afficher();
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
