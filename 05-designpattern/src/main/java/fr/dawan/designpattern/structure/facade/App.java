package fr.dawan.designpattern.structure.facade;

public class App {

    public static void main(String[] args) {
        LecteurAudioFacade laf = new LecteurAudioFacade();
        laf.jouer("Musique.mp3");

    }

}
