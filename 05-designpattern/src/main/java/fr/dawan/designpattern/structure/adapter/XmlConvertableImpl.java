package fr.dawan.designpattern.structure.adapter;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class XmlConvertableImpl<T> implements XmlConvertable<T> {

    @Override
    public String toXml(T obj) throws Exception {
        XmlMapper xmlMapper=new XmlMapper();
        return xmlMapper.writeValueAsString(obj);

    }

    @Override
    public T fromXml(String xml, Class<T> clazz) throws Exception {
        XmlMapper xmlMapper=new XmlMapper();
        return xmlMapper.readValue(xml.getBytes(),clazz);
    }

}
