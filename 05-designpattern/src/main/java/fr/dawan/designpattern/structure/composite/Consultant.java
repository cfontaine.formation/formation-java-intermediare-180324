package fr.dawan.designpattern.structure.composite;

public class Consultant extends Employee {

    public Consultant(String nom) {
        super(nom);
    }

    @Override
    public void afficher() {
       System.out.println(getNom());

    }

}
