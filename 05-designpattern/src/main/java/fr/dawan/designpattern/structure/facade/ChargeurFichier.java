package fr.dawan.designpattern.structure.facade;

public class ChargeurFichier {

    
    public void chargerFichier(String fichier) {
        System.out.println("Chargment du fichier " + fichier);
    }
}
