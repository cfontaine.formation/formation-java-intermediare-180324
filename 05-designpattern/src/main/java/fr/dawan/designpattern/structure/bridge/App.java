package fr.dawan.designpattern.structure.bridge;

public class App {

    public static void main(String[] args) {
       BasicRemote r=new BasicRemote();
       Device radio=new Radio();
        r.setDevice(radio);
        r.changeVolume(4);
    }

}
