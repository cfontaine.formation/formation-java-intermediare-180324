package fr.dawan.designpattern.structure.decorator;

public class VoitureLuxeDecorator extends VoitureDecorator {

    public VoitureLuxeDecorator(Voiture voiture) {
        super(voiture);
    }

    @Override
    public void assembler() {
        super.assembler();
        System.out.println("-- Ajouter les options luxes");
    }

}
