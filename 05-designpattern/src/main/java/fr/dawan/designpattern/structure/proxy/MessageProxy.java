package fr.dawan.designpattern.structure.proxy;

public class MessageProxy implements Message {
    
    private Message proxyMessage;

    public MessageProxy(Message proxyMessage) {
        this.proxyMessage = proxyMessage;
    }

    @Override
    public String getContent() {
        String msg=proxyMessage.getContent();
        
        String transformMsg=msg.toUpperCase();
        return transformMsg;
    }

}
