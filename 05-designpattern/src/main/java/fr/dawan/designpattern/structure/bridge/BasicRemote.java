package fr.dawan.designpattern.structure.bridge;

public class BasicRemote extends Remote {

    @Override
    public void changeVolume(int v) {
        device.setVolume(v);
    }

    @Override
    public void changeChannel(int c) {
        device.setChannel(c);

    }

    @Override
    public void switchOnOff() {
        if(device.isEnabled()) {
            device.turnOff();
        }
        else {
            device.turnOn();
        }

    }

}
