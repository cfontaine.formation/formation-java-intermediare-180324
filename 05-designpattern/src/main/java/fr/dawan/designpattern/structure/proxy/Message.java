package fr.dawan.designpattern.structure.proxy;

public interface Message {
    String getContent();
}
