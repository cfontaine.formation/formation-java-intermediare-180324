package fr.dawan.designpattern.structure.adapter;

public interface Target {

    String toJson(String xml) throws Exception;
}
