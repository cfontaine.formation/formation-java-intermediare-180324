package fr.dawan.designpattern.creation.abstractfactory;

public interface TextBox {
    void afficher();
}
