package fr.dawan.designpattern.creation.abstractfactory;

public interface Bouton {
    void afficher();
}
