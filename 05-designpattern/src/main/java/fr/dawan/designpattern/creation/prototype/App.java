package fr.dawan.designpattern.creation.prototype;

public class App {

    public static void main(String[] args) {
        Quiz quiz=new Quiz("Quiz Design pattern");
        Question q=new Question("à l'origine, quel est le nombre de design pattern ? ");
        q.getReponses().add(new Reponse("45",false));
        q.getReponses().add(new Reponse("12",false));
        q.getReponses().add(new Reponse("23",true));
        q.getReponses().add(new Reponse("42",false));
        quiz.addQuestions(q);
        System.out.println(quiz);
        
        try {
            Quiz quizClone=quiz.clone();
            quizClone.getQuestions().get(0).getReponses().get(0).setText("2");
            System.out.println(quiz);
            System.out.println(quizClone);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        

    }

}
