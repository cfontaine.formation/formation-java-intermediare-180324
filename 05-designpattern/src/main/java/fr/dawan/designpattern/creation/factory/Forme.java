package fr.dawan.designpattern.creation.factory;

public interface Forme {

    double calculSurface();
}
