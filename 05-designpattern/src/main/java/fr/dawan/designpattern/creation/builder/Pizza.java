package fr.dawan.designpattern.creation.builder;

public class Pizza {

    private String pate;
    private String sauce;
    private String garniture;
    

    public Pizza() {
    }


    private Pizza(String pate, String sauce, String garniture) {
        this.pate = pate;
        this.sauce = sauce;
        this.garniture = garniture;
    }

    
    public String getPate() {
        return pate;
    }
    
    
    public void setPate(String pate) {
        this.pate = pate;
    }
    public String getSauce() {
        return sauce;
    }
    public void setSauce(String sauce) {
        this.sauce = sauce;
    }
    public String getGarniture() {
        return garniture;
    }
    public void setGarniture(String garniture) {
        this.garniture = garniture;
    }


    @Override
    public String toString() {
        return "Pizza [pate=" + pate + ", sauce=" + sauce + ", garniture=" + garniture + "]";
    }
    
    
}
