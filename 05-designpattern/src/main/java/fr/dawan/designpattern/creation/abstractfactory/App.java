package fr.dawan.designpattern.creation.abstractfactory;



public class App {

    private Bouton bp;

    private TextBox tb;

    private App(UIFactory factory) {
        bp = factory.createBouton();
        tb = factory.createTextBox();
    }
    
    public void afficher() {
        bp.afficher();
        tb.afficher();
    }

    public static void main(String[] args) {
        UIFactory factory;
        String os=System.getProperty("os.name");
        if("Windows 10".equals(os)) {
            factory=new WindowsFactory();
        }else {
            factory=new HtmlFactory();
        }
        App application=new App(factory);
        application.afficher();
    }

}
