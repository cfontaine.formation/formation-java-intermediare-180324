package fr.dawan.designpattern.creation.abstractfactory;

public class HtmlBouton implements Bouton {

    @Override
    public void afficher() {
        System.out.println("Un bouton HTML");

    }

}
