package fr.dawan.designpattern.creation.builder;

public abstract class PizzaBuilder {

    protected Pizza pizza;
    
    public Pizza getPizza() {
        return pizza;
    }
    
    public void createNewPizza() {
        pizza=new Pizza();
    }
    
    public abstract PizzaBuilder buildPatte();
    public abstract PizzaBuilder buildSauce();
    public abstract PizzaBuilder buildGarniture();

    
}
