package fr.dawan.designpattern.creation.singleton;

public class App {

    public static void main(String[] args) {
        Pdg p1=Pdg.getInstance();
        
        //...
        Pdg p2=Pdg.getInstance();
        
        System.out.println(p1);
        System.out.println(p2);
//        Pdg p3=new Pdg();
//        System.out.println(p3);
    }

}
