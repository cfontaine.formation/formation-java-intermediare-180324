package fr.dawan.designpattern.creation.abstractfactory;

public class WindowsBouton implements Bouton {

    @Override
    public void afficher() {
        System.out.println("Un bouton windows");
    }

}
