package fr.dawan.designpattern.creation.abstractfactory;

public class WindowsFactory implements UIFactory {

    @Override
    public Bouton createBouton() {
        return new WindowsBouton();
    }

    @Override
    public TextBox createTextBox() {
        return new WindowTextBox();
    }

}
