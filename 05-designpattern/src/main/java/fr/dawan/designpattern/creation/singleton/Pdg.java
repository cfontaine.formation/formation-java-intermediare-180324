package fr.dawan.designpattern.creation.singleton;

public class Pdg {

    private volatile static Pdg instance;
    // volatile -> garantir que la varaible est visble et manipulable
    // au travers de plusieurs threads
    private String name;
    
    private Pdg() {

    }

    public synchronized static Pdg getInstance() {
        if(instance== null) {
            instance = new Pdg();
        }
        return instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
