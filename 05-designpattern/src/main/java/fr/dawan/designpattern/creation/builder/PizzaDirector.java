package fr.dawan.designpattern.creation.builder;

public class PizzaDirector {
    
    private PizzaBuilder pizzaBuilder;

    public void setPizzaBuilder(PizzaBuilder pizzaBuilder) {
        this.pizzaBuilder = pizzaBuilder;
    }

    public PizzaBuilder getPizzaBuilder() {
        return pizzaBuilder;
    }
    
    public Pizza getPizza() {
        return pizzaBuilder.getPizza();
    }

    public void construirePizza() {
        pizzaBuilder.createNewPizza();
        pizzaBuilder.buildPatte().buildSauce().buildGarniture();
    }
    
    public void construirePizza2() {
        pizzaBuilder.createNewPizza();
        pizzaBuilder.buildPatte().buildSauce();
    }
    
    // définir d'autre process de création
}
