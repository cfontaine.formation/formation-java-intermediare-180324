package fr.dawan.designpattern.creation.prototype;

import java.util.ArrayList;
import java.util.List;

public class Quiz implements Cloneable {

    private String titre;

    private List<Question> questions = new ArrayList<>();

    public Quiz(String titre) {
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void addQuestions(Question question) {
        questions.add(question);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Quiz [titre=").append(titre).append(", questions=").append(questions).append("]");
        return builder.toString();
    }

    @Override
    public Quiz clone() throws CloneNotSupportedException {
        // TODO Auto-generated method stub
        Quiz q=(Quiz)super.clone();
        q.questions=new ArrayList<>();
        for(Question question : questions) {
            q.questions.add((Question)question.clone());
        }
        return q;
    }
    
    

}
