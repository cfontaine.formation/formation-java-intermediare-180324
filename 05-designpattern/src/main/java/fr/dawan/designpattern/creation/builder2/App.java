package fr.dawan.designpattern.creation.builder2;

public class App {

    public static void main(String[] args) {
        Utilisateur u1 = new Utilisateur.UtilisateurBuilder("John", "Doe").build();
        System.out.println(u1);

        Utilisateur u2 = new Utilisateur.UtilisateurBuilder("John", "Doe").age(42)
                .adresse("46 rue des cannonniers 59800 LilLe").build();
        System.out.println(u2);
    }

}
