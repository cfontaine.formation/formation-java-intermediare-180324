package fr.dawan.designpattern.creation.builder2;

public class Utilisateur {

    private String prenom; // Obligatoire
    private String nom; // Obligatoire
    private int age; // le reste des attributs est optionnels
    private String email;
    private String telephone;
    private String adresse;

    // Utilisateur(String prenom, String nom, int age, String email, String
    // telephone, String adresse) {
    // Utilisateur(String prenom, String nom, int age) {}

    private Utilisateur() {
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getAdresse() {
        return adresse;
    }

    @Override
    public String toString() {
        return "Utilisateur [prenom=" + prenom + ", nom=" + nom + ", age=" + age + ", email=" + email + ", telephone="
                + telephone + ", adresse=" + adresse + "]";
    }

    // TODO vérifier utiliter static
    public static class UtilisateurBuilder {

        private Utilisateur u;

        public UtilisateurBuilder(String prenom, String nom) {
            u = new Utilisateur();
            u.prenom = prenom;
            u.nom = nom;
        }

        public UtilisateurBuilder age(int age) {
            u.age = age;
            return this;
        }

        public UtilisateurBuilder email(String email) {
            u.email = email;
            return this;
        }

        public UtilisateurBuilder telephone(String telephone) {
            u.telephone = telephone;
            return this;
        }

        public UtilisateurBuilder adresse(String adresse) {
            u.adresse = adresse;
            return this;
        }

        public Utilisateur build() {
            return u;
        }

    }

}
