package fr.dawan.designpattern.creation.abstractfactory;

public interface UIFactory {
    Bouton createBouton();
    TextBox createTextBox();
}
