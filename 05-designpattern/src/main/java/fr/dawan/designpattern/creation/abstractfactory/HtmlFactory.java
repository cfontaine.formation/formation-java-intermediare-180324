package fr.dawan.designpattern.creation.abstractfactory;

public class HtmlFactory implements UIFactory {

    @Override
    public Bouton createBouton() {

        return new HtmlBouton();
    }

    @Override
    public TextBox createTextBox() {
        return new HtmlTextBox();
    }

}
