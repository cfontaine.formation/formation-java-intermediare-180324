package fr.dawan.designpattern.creation.abstractfactory;

public class HtmlTextBox implements TextBox {

    @Override
    public void afficher() {
        System.out.println("Une textBox HTML"); 

    }

}
