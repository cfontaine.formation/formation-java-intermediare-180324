package fr.dawan.designpattern.creation.builder;

public class App {

    public static void main(String[] args) {
        PizzaDirector director= new PizzaDirector();
        
        PizzaNapolitaineBuilder builder=new PizzaNapolitaineBuilder();
        director.setPizzaBuilder(builder);
        director.construirePizza();
        Pizza pizza=director.getPizza();
        System.out.println(pizza);
    }

}
