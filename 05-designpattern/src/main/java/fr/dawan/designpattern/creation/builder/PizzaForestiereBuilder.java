package fr.dawan.designpattern.creation.builder;

public class PizzaForestiereBuilder extends PizzaBuilder {

    @Override
    public PizzaBuilder buildPatte() {
        pizza.setPate("epaisse");
        return this;
    }

    @Override
    public PizzaBuilder buildSauce() {
        pizza.setSauce("tomate");
        return this;
    }

    @Override
    public PizzaBuilder buildGarniture() {
        pizza.setGarniture("Jambon, champignon");
        return this;
    }

}
