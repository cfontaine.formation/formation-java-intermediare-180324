package fr.dawan.designpattern.comportement.observer;

public class Client implements Observer<Double> {
    
    private String nom;
    
    private double dernierPrixObserve;

    public Client(String nom) {
        this.nom = nom;
    }

    @Override
    public void update(Double prix) {
        System.out.println( nom + " notification de prix recue : " +prix);
        dernierPrixObserve=prix;
    }

    public String getNom() {
        return nom;
    }

    public double getDernierPrixObserve() {
        return dernierPrixObserve;
    }

}
