package fr.dawan.designpattern.comportement.chainofresposability;

public abstract class Staff {
    
    protected String nom;
    protected Staff successor;
    
    public Staff(String nom, Staff successor) {
        this.nom = nom;
        this.successor = successor;
    }
    
    public abstract void handleRequete(Requete req); 

}
