package fr.dawan.designpattern.comportement.observer;

public interface Subject<T> {
    void attach(Observer<T> observer);
    void detach(Observer<T> observer);
    // void notify();
    void notify(T obj);
}
