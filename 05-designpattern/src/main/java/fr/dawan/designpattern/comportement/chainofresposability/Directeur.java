package fr.dawan.designpattern.comportement.chainofresposability;

public class Directeur extends Staff {

    public Directeur(String nom, Staff successor) {
        super(nom, successor);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void handleRequete(Requete req) {
            System.out.println("votre requete a été traité par le  directeur" );
            req.setState(RequeteState.CLOSED);
        }

}
