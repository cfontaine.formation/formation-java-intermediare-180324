package fr.dawan.designpattern.comportement.chainofresposability;

public class DirecteurPedagogique extends Staff {

    public DirecteurPedagogique(String nom, Staff successor) {
        super(nom, successor);
    }

    @Override
    public void handleRequete(Requete req) {
        if(req.getType()==RequeteType.DIRECTEUR_PEDAGOGIQUE) {
            System.out.println("votre requete a été traité par le Directeur Pedagogique" );
            req.setState(RequeteState.CLOSED);
        }
        else if(successor!=null) {
            System.out.println("le Directeur Pedagogique a remonté votre requete" );
            successor.handleRequete(req);
        }

    }
}
