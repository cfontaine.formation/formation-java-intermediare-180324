package fr.dawan.designpattern.comportement.chainofresposability;

public enum RequeteState {
    OPENED, CLOSED 
}
