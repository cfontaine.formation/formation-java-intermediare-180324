package fr.dawan.designpattern.comportement.chainofresposability;

public class App {

    public static void main(String[] args) {
        Professeur p=new Professeur("prof", new DirecteurPedagogique("Directeur Pedagogique", new Directeur("Directeur", null)));
        p.handleRequete(new Requete(42, RequeteType.PROF ,RequeteState.OPENED, "req 1"));
        System.out.println("__________________________________________________");
        p.handleRequete(new Requete(42, RequeteType.DIRECTEUR_PEDAGOGIQUE ,RequeteState.OPENED, "req 2"));
        System.out.println("__________________________________________________");
        p.handleRequete(new Requete(42, RequeteType.DIRECTEUR ,RequeteState.OPENED, "req 3"));
    }

}
