package fr.dawan.designpattern.comportement.chainofresposability;

public class Professeur extends Staff {

    public Professeur(String nom, Staff successor) {
        super(nom, successor);
    }

    @Override
    public void handleRequete(Requete req) {
        if(req.getType()==RequeteType.PROF) {
            System.out.println("votre requete a été traité par le prof" );
            req.setState(RequeteState.CLOSED);
        }
        else if(successor!=null) {
            System.out.println("le prof a remonté votre requete" );
            successor.handleRequete(req);
        }

    }

}
