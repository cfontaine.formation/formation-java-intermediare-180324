package fr.dawan.designpattern.comportement.observer;

public interface Observer<T> {
    void update(T p);
}
