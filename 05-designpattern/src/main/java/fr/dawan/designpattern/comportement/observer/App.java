package fr.dawan.designpattern.comportement.observer;

public class App {

    public static void main(String[] args) {
       Produit pr1=new Produit("livre c#",25.0);
       Client c1=new Client("John Doe");
       Client c2=new Client("Jane Doe");
       Client c3=new Client("Peter Dragon");
       pr1.attach(c1);
       pr1.attach(c2);
       pr1.attach(c3);
       pr1.modifierPrix(19.0);
       pr1.detach(c2);
       pr1.modifierPrix(26.0);
    }

}
