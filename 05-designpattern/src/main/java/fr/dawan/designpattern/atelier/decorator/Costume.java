package fr.dawan.designpattern.atelier.decorator;

public interface Costume {
    String getDescription();

    double getPrix();
}
