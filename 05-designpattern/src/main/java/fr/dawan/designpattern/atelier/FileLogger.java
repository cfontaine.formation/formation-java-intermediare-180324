package fr.dawan.designpattern.atelier;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class FileLogger implements ILogger {

    @Override
    public void log(String message) throws Exception{
        try (BufferedWriter bw=new BufferedWriter(new FileWriter("application.log",true)))
        {
            bw.write("[Log]" +message +"\n");
        }

    }

}
