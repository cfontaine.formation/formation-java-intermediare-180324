package fr.dawan.designpattern.atelier.decorator;

public abstract class CostumeDecorator implements Costume {

    protected Costume costume;

    public CostumeDecorator(Costume costume) {
        this.costume = costume;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getDescription()).append(" prix=").append(getPrix());
        return builder.toString();
    }

}
