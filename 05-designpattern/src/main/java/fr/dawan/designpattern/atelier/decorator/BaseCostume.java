package fr.dawan.designpattern.atelier.decorator;

public class BaseCostume implements Costume {

    @Override
    public String getDescription() {
        return "costume de base";
    }

    @Override
    public double getPrix() {
        return 20.0;
    }



    
}
