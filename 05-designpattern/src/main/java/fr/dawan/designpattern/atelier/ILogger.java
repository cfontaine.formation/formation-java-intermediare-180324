package fr.dawan.designpattern.atelier;

public interface ILogger {
    void log(String message) throws Exception;
}
