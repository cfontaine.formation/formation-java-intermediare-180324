package fr.dawan.conceptionobjet.dependances.association;

public class App {

    public static void main(String[] args) {
        Employe e1=new Employe(1,"Jim profit");
        Employe e2=new Employe(2,"John Doe");
        
        Departement d1=new Departement("fusion et acquisition");

        d1.AddEmploye(e1);
        d1.AddEmploye(e2);
        System.out.println(d1);
    }

}
