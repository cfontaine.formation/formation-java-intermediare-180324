package fr.dawan.conceptionobjet.dependances.association;

// Dependance de type association (Association/Agrégation)
// -> s'étend sur toute la durée de vie d'un objet
public class A {

    private B b;

    public A(B b) {
        this.b = b;
    }
    // ...
}
