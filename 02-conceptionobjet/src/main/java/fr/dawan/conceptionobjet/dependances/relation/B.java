package fr.dawan.conceptionobjet.dependances.relation;

// Dépendance Relation (Dépendance)
// la plus faible dépendance entre objets
// limité dans le temps -> pendant l'éxécution de la méthode
public class B {
    
    public void methodeParam(A a) {
        a.methodeA(); // limité à du code partagé
    }
    
    public A methodeReturnA() {
        return new A();
    }

}
