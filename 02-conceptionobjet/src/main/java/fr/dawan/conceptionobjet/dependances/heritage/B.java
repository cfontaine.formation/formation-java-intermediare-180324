package fr.dawan.conceptionobjet.dependances.heritage;
// Dépendance héritage -> le lien de dépendance le plus important entre objet
// Tout changement sur la classe mère 
// aura une incidence sur les classes fille 
public class B extends A {

    public B(int valeur) {
        super(valeur);

    }

}
