package fr.dawan.conceptionobjet.dependances.association;

import java.util.ArrayList;
import java.util.List;

public class Departement {

    public String name;
    
    private List<Employe> employes=new ArrayList<>();

    public Departement(String name) {
        this.name = name;
    }
    
    public void AddEmploye(Employe e) {
        employes.add(e);
    }

    @Override
    public String toString() {
        return employes.stream().map(e -> e.toString())
                .reduce("Department"+ name +"\n",(a,b) -> a + b + "\n");
    }
    
    
}
