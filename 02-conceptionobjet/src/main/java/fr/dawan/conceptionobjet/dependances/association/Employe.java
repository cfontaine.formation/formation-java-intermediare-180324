package fr.dawan.conceptionobjet.dependances.association;

public class Employe {

    private int id;
    
    private String nom;

    public Employe(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Employe [id=" + id + ", nom=" + nom + "]";
    }
    
    
}
