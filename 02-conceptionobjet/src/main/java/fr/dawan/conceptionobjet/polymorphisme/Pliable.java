package fr.dawan.conceptionobjet.polymorphisme;

public interface Pliable {

    // depuis java 8
   default void plier() {
       System.out.println("plier ...");
   }
    
    void deplier();
}
