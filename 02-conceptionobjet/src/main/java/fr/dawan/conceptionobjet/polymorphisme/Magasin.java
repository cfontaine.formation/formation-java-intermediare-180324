package fr.dawan.conceptionobjet.polymorphisme;

public class Magasin {

    //  polymorphisme ad-hoc (à éviter)
    public static void acheter(Object p) {
        if (p instanceof Pliable pli) { // java 17 pas de cast
            System.out.println(pli);
        }
    }
    
    // polymorphisme par sous-typage
    public static void acheter(Pliable p) {
        System.out.println(p);
    }
    
    // polymorphisme par type paramètrique (avec un contrainte sur le type générique)
    public static <T extends Pliable >void acheterGenerique(T p){
        System.out.println(p);
    }
    
}
