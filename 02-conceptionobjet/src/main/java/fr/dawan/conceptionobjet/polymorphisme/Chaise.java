package fr.dawan.conceptionobjet.polymorphisme;

public class Chaise implements Pliable {

//    @Override
//    public void plier() {
//        System.out.println("Plier la chaise");
//
//    }

    @Override
    public void deplier() {
        System.out.println("Déplier la chaise");
    }

}
