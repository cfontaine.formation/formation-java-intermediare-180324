package fr.dawan.conceptionobjet.heritage;

public class AligatorAmerique extends Aligator {

    public AligatorAmerique(String nom) {
        super(nom);
    }

    @Override
    public void nager() {
        System.out.println("L'aligator nage en amérique");
    }
    
    

}
