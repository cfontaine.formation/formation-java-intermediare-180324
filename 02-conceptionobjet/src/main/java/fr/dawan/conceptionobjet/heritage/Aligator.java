package fr.dawan.conceptionobjet.heritage;

public class Aligator extends Animal {

    public Aligator(String nom) {
        super(nom);
    }

    @Override
    public void manger() {
       System.out.println("L'aligator mange");

    }

    @Override
    public void faireDuBruit() {
        System.out.println("L'aligator gronde");
    }
    
    public void nager() {
        System.out.println("l'aligator nage");
    }

}
