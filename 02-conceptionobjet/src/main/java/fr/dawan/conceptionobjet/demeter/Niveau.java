package fr.dawan.conceptionobjet.demeter;

import java.util.List;

public class Niveau {

    private List<EtudiantClasse> classes;

    public Niveau(List<EtudiantClasse> classes) {
        this.classes = classes;
    }
    
//    public List<EtudiantClasse> getClasses() {
//        return classes;
//    }

    public long compterEtudiants() {
        return classes.stream().map(c -> c.compteEtudiants())
                .reduce(0L,(a,b)-> a+b);
    }
}
