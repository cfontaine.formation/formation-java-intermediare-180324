package fr.dawan.conceptionobjet.demeter.atelier;

import java.util.List;

public class LiveurJournaux {
    
    private List<Client> clients;
    
    private double sommeCollecter;

    public LiveurJournaux(List<Client> clients) {
        this.clients = clients;
    }
    
    public void collectePaiement() {
        double prixJournal = 1;
        sommeCollecter=clients.stream()
                .map(c-> c.paiement(prixJournal))
                .reduce(sommeCollecter,(a,b) -> a + b);
    }

}
