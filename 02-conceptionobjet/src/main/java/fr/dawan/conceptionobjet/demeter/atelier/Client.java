package fr.dawan.conceptionobjet.demeter.atelier;

public class Client {

    private String nom;

    private Portefeuille portefeuille;

    public Client(String nom, Portefeuille portefeuille) {
        this.nom = nom;
        this.portefeuille = portefeuille;
    }

    public String getNom() {
        return nom;
    }

    public double paiement(double valeur) {
        return portefeuille.debiter(valeur);
    }

}
