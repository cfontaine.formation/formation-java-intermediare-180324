package fr.dawan.conceptionobjet.demeter.atelier;

public class Portefeuille {

    private double argent;

    public Portefeuille(double argent) {
        this.argent = argent;
    }

    public double getArgent() {
        return argent;
    }

    public double debiter(double valeur) {
        if (argent > valeur) {
            argent -= valeur;
            return valeur;
        }
        return 0;
    }
}
