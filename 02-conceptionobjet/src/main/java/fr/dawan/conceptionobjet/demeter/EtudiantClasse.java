package fr.dawan.conceptionobjet.demeter;

import java.util.List;

public class EtudiantClasse {

    private List<Etudiant> etudiants;

    public EtudiantClasse(List<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }

//    public List<Etudiant> getEtudiants() {
//        return etudiants;
//    }
    
    public long compteEtudiants() {
        return etudiants.size();
    }
    
}
