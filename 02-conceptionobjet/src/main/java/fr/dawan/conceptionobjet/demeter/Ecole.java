package fr.dawan.conceptionobjet.demeter;

import java.util.List;

// Loi de déméter : une méthode peut appeler:
// - l'objet lui-même
// - les paramètres, 
// - les variables locales
// - les attributs de l'objet

public class Ecole {
    private List<Niveau> niveaux;

    public Ecole(List<Niveau> niveaux) {
        this.niveaux = niveaux;
    }
    
// ne respecte le loi de demeter
//    public long compterEtudiants() {
//        long count =0;
//        for(var n : niveaux) {
//            for(var c :n.getClasses()) {
//                for( var s : c.getEtudiants()) {
//                    count++;
//                }
//            }
//        }
//        return count;
//    }
    
    public long compterEtudiants() {
        return niveaux.stream().map(n -> n.compterEtudiants())
        .reduce(0L,(a,b)-> a+b);
    }
}
