package fr.dawan.conceptionobjet.encapsulation;

public class App {
    public static void main(String[] args) {
        try {
            Eleve e = new Eleve("Jim", "pr");
            e.ajouterNote(8);
            e.ajouterNote(16);
            e.ajouterNote(12);
            System.out.println(e.moyenne());
            System.out.println(e);
            e.ajouterNote(-1);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }

    }
}
