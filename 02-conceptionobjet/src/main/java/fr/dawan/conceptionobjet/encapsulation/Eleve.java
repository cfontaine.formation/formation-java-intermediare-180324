package fr.dawan.conceptionobjet.encapsulation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Eleve {

    private String prenom;
    
    private String nom;
    
    private List<Integer> notes=new ArrayList<>();

    public Eleve(String prenom, String nom) {
        setPrenom(prenom);
        setNom(nom);
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }
    
    private void setPrenom(String prenom) {
        if(prenom==null )
        {
            throw new IllegalArgumentException("prenom incorrect");
        }
        this.prenom=prenom;
    }
    
    private void setNom(String nom) {
        if(nom==null || nom.length()<3)
        {
            throw new IllegalArgumentException("nom incorrect");
        }
        this.nom=nom;
    }

     
    public double moyenne() {
        return notes.stream().collect(Collectors.averagingDouble(n -> n));
    }
    
    public void ajouterNote(int note) {
        if(note<0 || note>20) {
            throw new IllegalArgumentException("Une note est comprise etre 0 et 20");
        }
        notes.add(note);
    }
    
    public void modifEtatCivil(String prenom, String nom) {
        setPrenom(prenom);
        setNom(nom);
    }

    @Override
    public String toString() {
        return "prenom=" + prenom + ", nom=" + nom + ", moyenne=" + moyenne();
    }
    
    
}
