package fr.dawan.conceptionobjet.telldontask.good;

public interface Repository<T, ID> {
    T findById(ID idCompte);

    void save(T t);
}
