package fr.dawan.conceptionobjet.telldontask.good;

public interface CompteBancaireService {

    void debiter(int idCompte, double montant) throws Exception;
}
