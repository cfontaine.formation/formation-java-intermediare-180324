package fr.dawan.conceptionobjet.telldontask.good;

public class CompteBancaire {

    private int id;
    
    private double solde;

    public CompteBancaire(int id, double solde) {
        this.id = id;
        this.solde = solde;
    }

    public void debiter(double montant) {
        if(solde< montant) {
            throw new IllegalArgumentException(" pas assez d'argent");
        }
        solde-=montant;
    }
}
