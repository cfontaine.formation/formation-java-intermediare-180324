package fr.dawan.conceptionobjet.telldontask.good;

public class CompteBancaireServiceImpl implements CompteBancaireService {

    private CompteBancaireRepository repo;
    
    public CompteBancaireServiceImpl(CompteBancaireRepository repo) {
        this.repo = repo;
    }

    @Override
    public void debiter(int idCompte, double montant) throws Exception{
        CompteBancaire cb= repo.findById(idCompte);
        cb.debiter(montant);
        repo.save(cb);

    }

}
