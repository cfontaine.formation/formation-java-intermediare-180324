package fr.dawan.conceptionobjet.telldontask.bad;

public class CompteBancaire {

    private int id;
    
    private double solde;

    public CompteBancaire(int id, double solde) {
        this.id = id;
        this.solde = solde;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
    
}
