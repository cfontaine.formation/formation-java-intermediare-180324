package fr.dawan.conceptionobjet.telldontask.bad;

public class CompteBancaireService {

    private CompteBancaireRepository repo;
    
    public void debiter(int idCompte, double montant) {
        CompteBancaire cb=repo.findById(idCompte);
        if(cb.getSolde()< montant) {
            throw new IllegalArgumentException(" pas assez d'argent");
        }
        cb.setSolde(cb.getSolde()-montant);
        repo.save(cb);
    }
}
