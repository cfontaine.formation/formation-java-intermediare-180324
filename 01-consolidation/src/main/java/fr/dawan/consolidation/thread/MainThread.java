package fr.dawan.consolidation.thread;

public class MainThread {

    public static void main(String[] args) {
        String mainName = Thread.currentThread().getName();

        Thread t1 = new Thread(() -> {
            String name = Thread.currentThread().getName();
            for (int i = 0; i < 100; i++) {
                System.out.println(name + " " + i);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread t2 = new Thread(() -> {
            String name = Thread.currentThread().getName();
            for (int i = 0; i < 100; i++) {
                System.out.println(name + " " + i);

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    
                }
            }
        });

        t1.setName("T1");
        // t1.setDaemon(true);
        // t1.setPriority(Thread.MIN_PRIORITY);
        t2.setName("T2");
        // t2.setDaemon(true);
        // t2.setPriority(Thread.MAX_PRIORITY);
        t1.start();
//        try {
//            t1.join();
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        t2.start();

        for (int i = 0; i < 100; i++) {
            System.out.println(mainName + " " + i);
        }
        t1.interrupt();
        System.out.println("Fin du main");
    }

}
