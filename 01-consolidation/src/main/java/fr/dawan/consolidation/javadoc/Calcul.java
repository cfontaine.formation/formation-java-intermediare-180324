package fr.dawan.consolidation.javadoc;

/**
 * Classe pour effectuer des calculs sur des entiers
 */
public class Calcul {

    /**
     *  Méthode qui permet de calculer la somme de 2 entiers
     * @author moi
     * @param a un entier
     * @param b un entier
     * @return la somme des 2 paramètres
     */
    public int somme (int a, int b) {
        return a+b;
    }
    
    /**
     *  Méthode qui permet de calculer la multiplication de 2 entiers
     * @param a un entier
     * @param b un entier
     * @return la multiplication des 2 paramètres
     */
    public int multiplication (int a, int b) {
        return a*b;
    }
}
