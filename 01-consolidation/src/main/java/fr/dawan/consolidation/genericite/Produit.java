package fr.dawan.consolidation.genericite;

@SuppressWarnings("serial")
public class Produit extends BaseDb implements Comparable<Produit> {

  //  private static final long serialVersionUID = 1L;
    
    private String description;
    
    private double prix;
    
    public Produit() {
        super(0L);
    }

    public Produit(String description, double prix) {
        super(0L);
        this.description = description;
        this.prix = prix;
    }

    public Produit(long id, String description, double prix) {
        super(id);
        this.description = description;
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Produit [id=" + getId() + ", description=" + description + ", prix=" + prix + "]";
    }

    @Override
    public int compareTo(Produit produit) {
        if(description.compareTo(produit.description)==0) {
            if(prix>produit.prix) {
                return 1;
            }
            else if(prix<produit.prix) {
                return -1;
            }
            else {
                return 0;
            }
        }
        return description.compareTo(produit.getDescription());
    }

}
