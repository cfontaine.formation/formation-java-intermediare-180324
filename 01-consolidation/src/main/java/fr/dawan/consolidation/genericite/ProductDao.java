package fr.dawan.consolidation.genericite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductDao implements Dao<Produit> {

    private final String url = "jdbc:mysql://localhost:3306/formation";
    private final String user = "root";
    private final String password = "";

    @Override
    public List<Produit> getAll() {
        List<Produit> lst = new ArrayList<>();
        try (Connection cnx = DriverManager.getConnection(url, user, password)) {
            PreparedStatement ps = cnx.prepareStatement("SELECT id,description,prix FROM produits");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new Produit(rs.getLong("id"), rs.getString("description"), rs.getDouble("prix")));
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return lst;
    }

    @Override
    public void insert(Produit elm) {
        try (Connection cnx = DriverManager.getConnection(url, user, password)) {
            PreparedStatement ps = cnx.prepareStatement("INSERT INTO produits(description,prix) VALUES (?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, elm.getDescription());
            ps.setDouble(2, elm.getPrix());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                elm.setId(rs.getLong(1));
            }
        } catch (SQLException e) {
            System.err.println("erreur conection");

        }

    }

}
