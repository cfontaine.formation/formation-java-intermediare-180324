package fr.dawan.consolidation.genericite;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ImportExportTools {

    public static <T> void toBin(String cheminFichier, T t) throws FileNotFoundException, IOException {
        try (BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(cheminFichier))) {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(t);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T fromBin(String cheminFichier) throws IOException, ClassNotFoundException {
        Object obj = null;
        try (BufferedInputStream fis = new BufferedInputStream(new FileInputStream(cheminFichier))) {
            ObjectInputStream ois = new ObjectInputStream(fis);
            obj = ois.readObject();
        }
        return (T) obj;
    }

    public static <T> void toCsv(String cheminFichier, List<T> lst) throws Exception {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(cheminFichier))) {
            boolean first = true;
            for (T elm : lst) {
                Class<?> clazz = elm.getClass();
                Field[] champs = clazz.getDeclaredFields();
                StringBuffer sb = new StringBuffer();
                if (first) {
                    StringBuffer ligne = new StringBuffer();
                    for (Field f : champs) {
                        ligne.append(f.getName());
                        ligne.append(";");
                    }
                    bw.write(ligne.toString());
                    bw.newLine();
                    first = false;
                }
                for (Field f : champs) {
                    f.setAccessible(true);
                    sb.append(f.get(elm).toString());
                    sb.append(";");
                }
                bw.write(sb.toString());
                bw.newLine();
            }
        }

    }

    public static <T> List<T> FromCsv(String cheminFichier, Class<T> clazz) throws Exception {
        List<T> lst = new ArrayList<T>();
        try (BufferedReader br = new BufferedReader(new FileReader(cheminFichier))) {
            br.readLine(); // on ignore la première ligne
            String line = null;
            while ((line = br.readLine()) != null) {
                String[] tab = line.split(";");
                T obj = clazz.getDeclaredConstructor().newInstance();
                Field[] fields = clazz.getDeclaredFields();
                int i = 0;
                for (Field f : fields) {
                    f.setAccessible(true);
                    Class<?> type = f.getType();
                    if (type.getName().equals("string")) {
                        f.set(obj, tab[i]);
                    } else if (type.getName().equals("double")) {
                        f.set(obj, Double.parseDouble(tab[i]));
                    }
                    i++;
                }
                lst.add(obj);
            }
        }
        return lst;
    }
}
