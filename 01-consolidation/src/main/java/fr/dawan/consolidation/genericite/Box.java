package fr.dawan.consolidation.genericite;

// Classe générique
public class Box<T> {
    private T contenu;

    public Box(T contenu) {
        this.contenu = contenu;
    }

    public T getContenu() {
        return contenu;
    }

    public void setContenu(T contenu) {
        this.contenu = contenu;
    }

    // Type générique sur une méthode
    public <U> void afficherContenu(U prefix) {
        System.out.println(prefix.toString() + contenu);
    }

}
