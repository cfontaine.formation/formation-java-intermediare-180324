package fr.dawan.consolidation.genericite;

import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
        // Généricité
        // sur les classes
        Box<String> box1 = new Box<>("Hello world;");
        System.out.println(box1.getContenu());

        Box<Integer> box2 = new Box<>(42);
        System.out.println(box2.getContenu());

        // sur les méthodes
        // box1.<String>afficherContenu("Contenu="); // implicite
        box1.afficherContenu("Contenu=");
        box2.afficherContenu(1);

        // Contrainte sur les types génériques
        Dao<Produit> dao = new ProductDao();
        Produit p1 = new Produit("Stylo", 3.5);
        dao.insert(p1);
        List<Produit> lst = dao.getAll();
        System.out.println(lst);

        // Instantiation d'un type générique
        try {
            String str = create(String.class);
            Produit p2 = create(Produit.class);
            System.out.println(p2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Générique WildCard ?
        // type<?>
        List<String> lstR = Arrays.asList("hello", "world", "bonjour");
        // List<Object> lstA=lstR;

        List<?> lstA = lstR;
        // lstA.add("qwerty"); // erreur add à pour paramètre un type T :add(?)
        System.out.println(lstA.size());

        // Contrainte sur ?
        List<Integer> lstI = Arrays.asList(1, 4, 6, 9);
        // List<? extends Number> lstN=lstR; // String n'hérite pas de Number
        List<? extends Number> lstN = lstI;

        List<BaseDb> lstB = Arrays.asList(new Produit("tv", 350.0));
        // List<? super Produit> lstP=lstR;
        List<? super Produit> lstP = lstB;
        System.out.println(lstP.size());

        try {
            ImportExportTools.toBin("p1.bin", p1);
            Produit p2 = ImportExportTools.fromBin("p1.bin");
            System.out.println(p2);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static <T> T create(Class<T> clazz) throws Exception {
        // return clazz.newInstance(); // deprecated depuis java 9
        return clazz.getDeclaredConstructor().newInstance();
    }
}
