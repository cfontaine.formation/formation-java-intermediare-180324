package fr.dawan.consolidation.genericite;

import java.io.Serializable;

public abstract class BaseDb implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    public BaseDb(long id) {
        super();
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
