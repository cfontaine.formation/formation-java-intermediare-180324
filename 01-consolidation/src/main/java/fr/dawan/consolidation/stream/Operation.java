package fr.dawan.consolidation.stream;

@FunctionalInterface
public interface Operation {

    int calcul(int a, int b);

}
