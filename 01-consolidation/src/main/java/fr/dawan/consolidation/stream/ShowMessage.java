package fr.dawan.consolidation.stream;

public class ShowMessage {

    public void afficherMsg()
    {
        System.out.println("Référence de méthode d'instance");
    }

    @Override
    public String toString() {
        return "Référence de méthode constructeur";
    }
    
    
}
