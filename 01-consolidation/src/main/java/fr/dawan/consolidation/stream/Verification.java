package fr.dawan.consolidation.stream;

public interface Verification {
    boolean check(String str);
}
