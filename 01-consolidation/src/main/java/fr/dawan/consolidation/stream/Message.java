package fr.dawan.consolidation.stream;

@FunctionalInterface
public interface Message {
    void afficher();
}
