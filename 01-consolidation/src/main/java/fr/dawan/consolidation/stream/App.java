package fr.dawan.consolidation.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.dawan.consolidation.genericite.Produit;

public class App {

    public static void main(String[] args) {

        // 1. Une classe qui implemente l'interface
        Operation op1 = new Somme();
        System.out.println(traitement(1, 2, op1));

        // 2. Classe Annonyme
        System.out.println(traitement(1, 2, new Operation() {

            @Override
            public int calcul(int a, int b) {
                return a + b;
            }
        }));

        // 3. avec Java 8 les expresion lambda
        System.out.println(traitement(1, 2, (int a, int b) -> {
            return a + b;
        }));

        // le type des paramètres est déduit à partir des paramètres de l'interface

        // plusieurs paramètres (v1,valeur,e) -> ...
        // 1 paramètre a -> ...
        // pas de paramètre () -> ...
        System.out.println(traitement(1, 2, (a, b) -> {
            return a + b;
        }));

        // corps de méthode
        // plusieurs lignes (...) -> { ;}
        // une lignes (...) ->
        System.out.println(traitement(1, 2, (a, b) -> a + b));

        System.out.println(traitement(4, 2, (v1, v2) -> v1 * v2));
        System.out.println(traitement(4, 2, (v1, v2) -> v1 - v2));

        Operation op = (v1, v2) -> v1 - v2;
        System.out.println(traitement(4, 2, op));

        Verification ve = s -> {
            String tmp = s.trim().toLowerCase();
            return tmp.equals(new StringBuilder(tmp).reverse().toString());
        };
        verif("radar", ve);
        verif("bonjour", ve);

        List<String> lstStr = Arrays.asList("ddd", "Azerty", "ccc", "aaa", "bbb");
        lstStr.sort((s1, s2) -> s1.compareTo(s2));
        System.out.println(lstStr);

        String sep = "-";
        Arrays.asList("ddd", "Azerty", "ccc", "aaa", "bbb").forEach(s -> System.out.print(s + sep));
        System.out.println();

        // Interface fonctionnel
        verifInterF("sos", s -> {
            String tmp = s.trim().toLowerCase();
            return tmp.equals(new StringBuilder(tmp).reverse().toString());
        });

        List<Integer> lst = Arrays.asList(5, 4, 1, 8);
        lst.sort((a, b) -> a > b ? 1 : -1);
        System.out.println(lst);

        // Référence de méthode

        // Référence de méthode statique
        // - avec une expression lambda
        Message msg1 = () -> afficherMessage();
        msg1.afficher();
        // - référence de méthode: nom_classe::nom_methode_static
        Message msg2 = App::afficherMessage;
        msg2.afficher();

        // référence de méthode d'instance
        ShowMessage obj = new ShowMessage();
        // - avec une expression lambda
        Message msg3 = () -> obj.afficherMsg();
        msg3.afficher();
        // - référence de méthode: nom_ojet::nom_methode
        Message msg4 = obj::afficherMsg;
        msg4.afficher();

        // référence de méthode constructeur
        // - avec une expression lambda
        Supplier<ShowMessage> msg5 = () -> new ShowMessage();
        System.out.println(msg5.get());
        // - référence de méthode: nom_classe::new
        Supplier<ShowMessage> msg6 = ShowMessage::new;
        System.out.println(msg6.get());

        // Méthode forEach
        List<String> lstPrenom = Arrays.asList("Jim", "Peter", "Michel", "Yves");
        for (var p : lstPrenom) {
            System.out.println(p.toUpperCase());
        }

        lstPrenom.forEach(prenom -> System.out.println(prenom.toUpperCase()));

        lstPrenom.forEach(System.out::println);

        // Stream
        List<Integer> lstInt = Arrays.asList(-5, 4, 2, 0, -3, 1, -6, 2, 9, -8);
        lstInt.stream().map(i -> i * 2).filter(x -> x >= 0 && x <= 10).sorted().forEach(System.out::println);
        System.out.println("____");
        lstInt.stream().distinct().skip(3).forEach(System.out::println);

        System.out.println("count=" + lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).sorted()
                .collect(Collectors.counting()));
        List<Integer> lstRes = lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).sorted()
                .collect(Collectors.toList());

        lstRes = lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).sorted().toList();// -> List immuable
        System.out.println(lstRes);
        int somme = lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).reduce(0, (a, b) -> a + b);
        System.out.println(somme);

        System.out.println(lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).sorted().findFirst().get());
        System.out
                .println(lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).sorted().anyMatch(i -> i > 10)); // true
        System.out
                .println(lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).sorted().anyMatch(i -> i > 20)); // false

        System.out
                .println(lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).sorted().allMatch(i -> i > 10)); // false
        System.out.println(lstInt.stream().distinct().map(i -> i * 2).filter(f -> f > 0).sorted().allMatch(i -> i > 1)); // true

        List<Produit> lstProd = new ArrayList<>();
        lstProd.add(new Produit("Stylo", 4.0));
        lstProd.add(new Produit("Livre react", 50.0));
        lstProd.add(new Produit("Livre git", 30.0));
        lstProd.add(new Produit("Livre java", 40.0));
        lstProd.add(new Produit("Livre c#", 45.0));
        lstProd.add(new Produit("Souris", 25.0));
        lstProd.add(new Produit("Clavier", 9.0));

        System.out.println(lstProd.stream().filter(p -> p.getPrix() <= 40.0 && p.getDescription().startsWith("Livre"))
                .sorted().toList());

        System.out.println(lstProd.stream().filter(p -> !p.getDescription().startsWith("Livre")).map(p -> p.getPrix())
                .reduce(0.0, (v1, v2) -> v1 + v2));
        //
        List<Integer> notes = Arrays.asList(6, 14, 7, 10, 12);
        System.out.println(notes.stream().collect(Collectors.averagingDouble(n -> n)));
        //
        List<String> mots = Arrays.asList("Bonjour", "java", "c++", "hello", "world", "c#", "c", "html");
        System.out.println(mots.stream().filter(m -> m.length() % 2 == 0).count());
        System.out.println(mots.stream().filter(m -> m.charAt(0) < 'i').sorted().reduce((a, b) -> a + " " + b).get());
        mots.stream().sorted((m1, m2) -> m1.length() > m2.length() ? -1 : 1).limit(3).forEach(System.out::println);
        System.out.println(factoriel(3));
        System.out.println(factoriel(4));

        // Optionnal
        // Création
        Optional<String> optVide = Optional.empty();
        Optional<String> opt1 = Optional.of("Hello");
        Optional<String> optVide2 = Optional.ofNullable(null);
        Optional<String> opt2 = Optional.ofNullable("World");

        // Tester le contenu
        System.out.println(optVide.isEmpty()); // true
        System.out.println(optVide.isPresent()); // false
        System.out.println(opt1.isEmpty()); // false
        System.out.println(opt1.isPresent()); // true

        System.out.println(opt1.get()); // Hello
        // System.out.println(optVide.get()); // -> exception
        if (optVide.isPresent()) {
            System.out.println(optVide.get()); // -> exception
        }

        // ifPresent -> consumer
        opt1.ifPresent(s -> System.out.println(s));
        opt1.ifPresent(System.out::println);

        // orElse valeur par défaut
        System.out.println(optVide.orElse("La chaine est vide"));

        System.out.println(optVide.orElseGet(() -> "C'est vide"));

        try {
            System.out.println(optVide.orElseThrow(Exception::new));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int traitement(int i1, int i2, Operation op) {
        return op.calcul(i1, i2);
    }

    public static void verif(String str, Verification v) {
        if (v.check(str)) {
            System.out.println("Ok");
        }
    }

    public static void verifInterF(String str, Predicate<String> v) {
        if (v.test(str)) {
            System.out.println("Ok");
        }
    }

    public static void afficherMessage() {
        System.out.println("Référence de méthode statique");
    }

    public static int factoriel(int n) {
        if (n < 1) {
            return 1;
        }
        return Stream.iterate(1, i -> i + 1).limit(n).reduce(1, (a, b) -> a * b);
    }

}
