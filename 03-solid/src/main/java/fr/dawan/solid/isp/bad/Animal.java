package fr.dawan.solid.isp.bad;

public interface Animal {
    void manger();

    void deplacer();

    void respirer();

    void nager();

    void courir();
}
