package fr.dawan.solid.isp.ok;

public interface AnimalAquatique extends Animal{
    void nager();
}
