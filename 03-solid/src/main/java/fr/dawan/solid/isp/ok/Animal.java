package fr.dawan.solid.isp.ok;

public interface Animal {
    void manger();

    void deplacer();

    void respirer();

//    void nager();
//
//    void courir();
}
