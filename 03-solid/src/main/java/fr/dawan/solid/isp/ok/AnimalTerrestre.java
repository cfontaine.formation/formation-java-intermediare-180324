package fr.dawan.solid.isp.ok;

public interface AnimalTerrestre extends Animal{
    void courir();
}
