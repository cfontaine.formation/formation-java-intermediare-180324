package fr.dawan.solid.ocp.bad;

// ne respecte pas le principe Open Closed
// on devra ajouter une nouvelle méthode de calcul de surface
// pour chaque nouvelle forme => la classe n'est pas fermé à la modification 
public class CalculSurface {

    public double calculSurfaceRectangle(Rectangle rectangle) {
        return rectangle.getHauteur()*rectangle.getLongueur();
    }
    
    
    public double calculSurfaceCercle(Cercle cercle) {
        return cercle.getRayon()*cercle.getRayon()*Math.PI;
    }
}
