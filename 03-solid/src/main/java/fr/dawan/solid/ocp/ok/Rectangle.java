package fr.dawan.solid.ocp.ok;

public class Rectangle implements Forme {
    private double longueur;
    private double hauteur;

    public Rectangle(double longueur, double hauteur) {
        this.longueur = longueur;
        this.hauteur = hauteur;
    }

    public double getLongueur() {
        return longueur;
    }

    public double getHauteur() {
        return hauteur;
    }

    @Override
    public double calculSurface() {
        return longueur * hauteur;
    }

}
