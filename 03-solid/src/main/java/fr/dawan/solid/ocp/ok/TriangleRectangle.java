package fr.dawan.solid.ocp.ok;

public class TriangleRectangle extends Rectangle {

    public TriangleRectangle(double longueur, double hauteur) {
        super(longueur, hauteur);
    }

    @Override
    public double calculSurface() {
        return super.calculSurface()/2.0;
    }

}
