package fr.dawan.solid.ocp.ok;

public interface Forme {
    double calculSurface();
}
