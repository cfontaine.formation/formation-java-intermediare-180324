package fr.dawan.solid.srp;

import java.time.LocalDate;

public class Employee {

    private long id;
    
    private String nom;
    
    private String prenom;
    
    private String adresse;
    
    private LocalDate dateArrive;
    
    // Ne respecte pas LE single responsability principe
    // Ces 3 méthodes ne sont pas de la responsabilité de l'employé
    // -> On a créer d'autre classe
//    public boolean promotionAnciennete() {
//        // traitement promotion
//        return false;
//    }
//    
//    public double calculImpotAnnee() {
//        // calcul impot
//        return 0.0;
//    }
    
//    public void save() {
//        
//    }
    
    
}
