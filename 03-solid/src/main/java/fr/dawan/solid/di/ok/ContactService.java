package fr.dawan.solid.di.ok;

public class ContactService {
    
    private ContactDao dao;
    
    // 1 - injection de dépendance via constructeur
    public ContactService(ContactDao dao) {
        this.dao = dao;
    }
    
//    public Contact getById(int id) {
//        return dao.findById();
//    }


    // 2 - Injection de dépendance via les paramètres de la méthode
//    public ContactService() {
//    }
//    
    
    public Contact getById(int id,ContactDao dao) {
        return dao.findById();
    }
    
    // 3 - Injection via le setter
    
    public ContactService() {
    }
    
    
    public Contact getById(int id) {
        return dao.findById();
    }

    public void setDao(ContactDao dao) {
        this.dao = dao;
    }
    
}
