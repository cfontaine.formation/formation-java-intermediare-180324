package fr.dawan.solid.di.ok;

public interface ContactDao {
    Contact findById();
    
    // ...
}
