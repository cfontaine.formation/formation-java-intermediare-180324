package fr.dawan.solid.di.bad;
// Le principe d'inversion de dépendence -> réduire le couplage entre les composants d'un système
// On va dépendre des abstraction plutot qque de l'implémentation
public class ContactService {

    public Contact getById(int id) {
        ContactDao dao=new ContactDao(); // Le service dépend de l'implémentation de la couche DAO
        return dao.findById();
    }
}
