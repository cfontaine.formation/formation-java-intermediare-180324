package fr.dawan.solid.liskov.ok;

public abstract class MoyenTransportMotorise extends MoyenTransport {

    private Engine engine;

    @Override
    public void startMoving() {
        engine.startEngine();
        
    }
    
    
    
}
