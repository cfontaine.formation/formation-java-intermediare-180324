package fr.dawan.solid.liskov.bad;

public class Velo extends MoyenTransport {

    @Override
    public void startEngine() {
        // On ne peut pas implémenter cette méthode
        // pas de moteur
    }

    // Le principe de liskov n'et pas respecté
    // un object d'un sous-type de Transport peut remplacer
    // un objet de Transport
    
    // Transport c=new Velo();
    // c.startEngine();
    
}
